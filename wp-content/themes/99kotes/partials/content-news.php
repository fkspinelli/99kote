<div class="container">
  <div class="row">
    <div class="col-sm-4 col-sm-push-4">
      <a href="https://99kote.globus7.com.br/CadastroUsuario" class="btn btn-success btn-block btn-xg">CADASTRE-SE</a>
    </div>
  </div>
</div>
<div class="block def-cl-1">
  <div class="container">
    <div class="row">
      <div class="col-sm-10 col-sm-push-1 text-center">
        <h2>Compradores</h2>
        <div class="box-cta">
          <h3>Não é cliente do software Globus? </h3>
          <p>Clique abaixo e solicite um contato</p>
          <?php echo do_shortcode('[contact-form-7 id="19" title="Contatos Solicitados"]'); ?>
        </div>
      </div>
    </div>
  </div>
</div>
