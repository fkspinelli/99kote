<div class="block bg-f1f1f1">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="text-center">
          <h2>+ 2000 EMPRESAS ESPERANDO POR VOCÊ!</h2>
          <h3>Toda rede de clientes da BgmRodotec pronta para fazer negócio com você!</h3>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="logos logos-6">
          <?php query_posts(array('post_type'=>'empresas')); while(have_posts()): the_post(); ?>
            <div class="logos-box">
              <a target="_blank" href="<?php echo CFS()->get('link'); ?>">
                <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail')[0]; ?>" alt="<?php echo get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true); ?>">
              </a>
            </div>
          <?php endwhile; wp_reset_query(); ?>
        </div>
      </div>
    </div>
    <div class="share">
      <div class="share-group">
        <a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//localhost/99kote/home.html" target="_blank" class="btn btn-default btn-facebook"><i class="ion-social-facebook"></i></a>
        <a href="https://plus.google.com/share?url=http%3A//localhost/99kote/home.html" target="_blank" class="btn btn-default btn-googlepush"><i class="ion-social-googleplus"></i></a>
        <a href="https://www.linkedin.com/shareArticle?mini=true&url=http%3A//localhost/99kote/home.html&title=&summary=&source=" target="_blank" class="btn btn-default btn-linkedin"><i class="ion-social-linkedin-outline"></i></a>
      </div>
    </div>
  </div>
</div>
