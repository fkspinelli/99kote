<?php
$args = array('post_type'=>'banners', 'posts_per_page'=>'1');
query_posts($args);
while(have_posts()): the_post(); ?>
<div class="banner">
  <div class="container">
    <div class="row row-eq-height">
      <div class="col-sm-6">
        <img src="<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium')[0]; ?>" alt="<?php echo get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', true); ?>" class="img-banner">
      </div>
      <div class="col-sm-6">
        <div class="content">
          <h1><?php the_title(); ?></h1>
          <?php the_content(); ?>
          <a href="https://99kote.globus7.com.br/CadastroUsuario" class="btn btn-lg btn-success">VENDA AGORA!</a>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endwhile; wp_reset_query(); ?>
