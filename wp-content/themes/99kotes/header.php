<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title(); ?></title>
	<link type="image/x-icon" title="99 Kote" rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.png">
  	<?php wp_head(); ?>
  	<?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false): ?>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	 
	  ga('create', 'UA-100357046-1', 'auto');
	  ga('send', 'pageview');
	 
	</script>
	<?php endif; ?>
</head>
<body>
	<header>
		<nav class="navbar navbar-default">
		 	<div class="container">
		    	<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
		      		<a class="navbar-brand" href="<?php echo bloginfo('url'); ?>">
		      			<img src="<?php bloginfo('template_url'); ?>/img/logo-99-kote.png" alt="99 Kote">
		      		</a>
		      	</div>
		      	<div class="collapse navbar-collapse" id="navbar">
				    <?php
                        wp_nav_menu( array(
                            'theme_location'              => 'primary',
                            'menu_class'       => 'nav navbar-nav',
                            'menu_id'         => 'menu_id',
                            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            'depth'             => 0,
                            'container'       => '',
                            'container_class' => false,
                            
                        ));
                    ?>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="https://99kote.globus7.com.br/">Login</a></li>
						<li><a href="https://99kote.globus7.com.br/CadastroUsuario" class="btn btn-success">CADASTRE-SE</a></li>
					</ul>
			    </div>
		  	</div>
		</nav>
	</header>
