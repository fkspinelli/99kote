<footer>
  <?php $page = get_post(11); ?>
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-md-2 col-sm-3">
          <a href="<?php echo bloginfo('url'); ?>">
            <img src="<?php bloginfo('template_url'); ?>/img/logo-99-kote.png" alt="99 Kote">
          </a>
        </div>
        <div class="col-lg-4 col-md-5 col-sm-6">
          <span class="address"><?php echo CFS()->get('endereco', $page->ID); ?></span>
        </div>
        <div class="col-lg-3 col-md-2 col-sm-3">
          <span class="phone">Telefone:</span>
          <span class="phone"><?php echo CFS()->get('telefone', $page->ID); ?></span>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="social">
            <a href="https://www.linkedin.com/company-beta/22298925" target="_blank"><i class="icon icon-social-linkedin-circle"></i></a>
            <a href="https://www.facebook.com/99kote/" target="_blank"><i class="icon icon-social-facebook-circle"></i></a>
            <a href="https://www.youtube.com/channel/UCRjTAnjjA3znI9vupck1adg" target="_blank"><i class="icon icon-social-youtube-circle"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-middle">
    <div class="container">
      <?php
          wp_nav_menu( array(
              'theme_location'              => 'primary',
              'menu_class'       => '',
              'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
              'depth'             => 0,
              'container'       => '',
              'container_class' => false,
              
          ));
      ?>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">Copyright © 2017 BgmRodotec SA. Todos os direitos reservados.</div>
  </div>
<?php wp_footer(); ?>
</footer>
</body>
</html>
