$(document).ready(function(){
	$(window).on('load resize', function(){
		$('.banner.inter').css({'height':$(window).height()-$('.navbar').height()});
	});

	$('.sub-menu').addClass('dropdown-menu');

	$('#slider-brands').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		infinite: true,
		dots: true,
		arrows: false,
		responsive: [
			{
				breakpoint: 991,
				settings: {
					slidesToShow: 4
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 3
				}
			}
		]
	});

	$('#slider-screens').slick({
		autoplay: true,
		autoplaySpeed: 2000,
		infinite: true,
		dots: true,
		asNavFor: '#slider-screens-for',
		prevArrow: $('#slider-screens-prev'),
		nextArrow: $('#slider-screens-next')
	});

	 $('#slider-screens-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: '#slider-screens'
	});

	$('#slider-screens-2').slick({
		autoplay: true,
		autoplaySpeed: 10000,
		infinite: true,
		dots: false,
		asNavFor: '#slider-screens-for-2',
		prevArrow: $('#slider-screens-prev-2'),
		nextArrow: $('#slider-screens-next-2')
	});

	$('#slider-screens-for-2').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: '#slider-screens-2'
	});

	// mask
	$('.cnpj').mask('99.999.999/9999-99');
});