<?php
/**
* Setup basico do tema
**/
function kotes_setup(){
  add_theme_support( 'post-thumbnails' );
  #add_image_size( 'banner-image', 633, 527, true );
  register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'twentyfifteen' ),
		'social'  => __( 'Social Links Menu', 'twentyfifteen' ),
	) );
}
add_action( 'after_setup_theme', 'kotes_setup' );
/**
* Setup scripts do tema
**/
function kotes_scripts()
{
  wp_enqueue_style( 'kotes-slick', get_template_directory_uri() . '/css/slick.css' );
  wp_enqueue_style( 'kotes-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css' );
  wp_enqueue_style( 'kotes-exo', get_template_directory_uri() . '/css/exo.css' );
  wp_enqueue_style( 'kotes-ionicons', get_template_directory_uri() . '/css/ionicons.min.css' );
  wp_enqueue_style( 'kotes-beauty-fonts', get_template_directory_uri() . '/css/beauty-font.css' );
  wp_enqueue_style( 'kotes-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css' );
  wp_enqueue_style( 'kotes-build', get_template_directory_uri() . '/css/build.css' );

  wp_enqueue_style( 'kotes-index', get_template_directory_uri() . '/css/index.css' );
  wp_enqueue_style( 'kotes-responsive', get_template_directory_uri() . '/css/responsive.css' );

  wp_enqueue_script( 'kotes-jquery', get_template_directory_uri() . '/js/jquery.min.js', array(), false, true );
  wp_enqueue_script( 'kotes-slick', get_template_directory_uri() . '/js/slick.min.js', array(), false, true );
  wp_enqueue_script( 'kotes-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), false, true );
  wp_enqueue_script( 'kotes-mask', get_template_directory_uri() . '/js/jquery.mask.js', array(), false, true );

  wp_enqueue_script( 'kotes-index', get_template_directory_uri() . '/js/index.js', array(), false, true );
}
add_action( 'wp_enqueue_scripts', 'kotes_scripts' );

function create_post_types()
{
  register_post_type( 'banners',
    array(
      'labels' => array(
        'name' => __( 'Banners' ),
        'singular_name' => __( 'Banner' ),
        'add_new_item'  => __( 'Adicionar novo banner'),
      ),
      'public' => true,
      'has_archive' => true,
      'menu_position'         => 1,
      'supports' => array('title','thumbnail','editor')
    )
  );

  register_post_type( 'empresas',
    array(
      'labels' => array(
        'name' => __( 'Empresas' ),
        'singular_name' => __( 'Banner' ),
        'add_new_item'  => __( 'Adicionar nova empresa'),
      ),
      'public' => true,
      'has_archive' => true,
      'menu_position'         => 2,
      'supports' => array('title','thumbnail')
    )
  );
}
add_action( 'init', 'create_post_types' );
