<?php get_header(); ?>
<section>
  <?php get_template_part('partials/content', 'banner');  $page = get_post(11); ?>
  <div class="brands">
    <div class="container">
      <div id="slider-brands" class="slider">
        <?php foreach (CFS()->get('logo_carrosel', $page->ID) as $logo) :  ?>
          <div><img src="<?php echo wp_get_attachment_url( $logo['logo_imagem'] ); ?>" alt="<?php echo get_post_meta($logo['logo_imagem'], '_wp_attachment_image_alt', true); ?>"></div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
  <div class="homevideo" style="background-image: url(<?php bloginfo('template_url'); ?>/img/bg-video.jpg);">
    <div class="container text-center">
      <h2>Assista ao Vídeo e conheça os benefícios do <b>99Kote</b></h2>
      <div class="row">
        <div class="col-sm-8 col-sm-push-2">
          <video width="400" poster="<?php bloginfo('template_url'); ?>/img/poster.jpg" controls>
            <source src="<?php bloginfo('template_url'); ?>/video/99Kote-A-Evolucao-do-B2B.mp4" type="video/mp4">
            Seu navegador não suporta vídeo HTML5.
          </video>
        </div>
      </div>
    </div>
  </div>
  <div class="block">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <img src="<?php bloginfo('template_url'); ?>/img/logo-99-kote-2.png" alt="99 Kote" class="img-center">
          <div style="margin-bottom: 60px;"></div>
        </div>
      </div>
      <div class="row row-eq-height row-eq-height-checklist">
        <div class="col-sm-6">
          <div class="checklist checklist-cl-1 box">
            <?php echo CFS()->get('compradores', $page->ID); ?>
            <h4>Não possui ERP Globus?</h4>
            <a href="/compradores/" class="btn btn-info text-uppercase">Saiba mais</a>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="checklist checklist-cl-2 box">
            <?php echo CFS()->get('fornecedores', $page->ID); ?>
            <a href="/fornecedores/" class="btn btn-info text-uppercase">Saiba mais</a>
            <a href="https://99kote.globus7.com.br/CadastroUsuario" class="btn btn-success text-uppercase">CADASTRE-SE</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="block def-cl-1" style="padding-bottom: 0;">
    <div class="container">
      <div class="item-box">
        <div class="row">
          <div class="col-sm-12">
            <h2>Perdeu aquela venda por esquecer de enviar a cotação?</h2>
            <h3>Com o 99Kote isso não vai acontecer. Veja esse e outros benefícios para vendedores. </h3>
          </div>
        </div>
        <div class="itens">
          <div class="item">
            <i class="ion-android-notifications"></i>
            <span>Recebe alertas imediatos para cada recebimento de cotação ou de pedido</span>
          </div>
          <div class="item">
            <i class="ion-ios-list"></i>
            <span>Tem Dashboards com fácil visualização da quantidade de cotação a expirar e das cotações convertidas em pedidos</span>
          </div>
          <div class="item">
            <i class="ion-eye"></i>
            <span>Consegue visualizar e pesquisar as cotações e pedidos</span>
          </div>
          <div class="item">
            <i class="ion-document-text"></i>
            <span>Faz a importação ou cadastro de catálogo de peças (com vigência de preço e atualização automática)</span>
          </div>
          <div class="item">
            <i class="ion-android-checkbox"></i>
            <span>Obtém o preenchimento automático das cotações</span>
          </div>
          <div class="item">
            <i class="ion-calculator"></i>
            <span>Cálculo automático dos impostos</span>
          </div>

          <div class="item">
            <i class="ion-arrow-swap"></i>
            <span>Integra com outros sistemas</span>
          </div>
          <div class="item">
            <i class="ion-ios-people"></i>
            <span>Participa em cotações públicas</span>
          </div>
          <div class="item">
            <i class="ion-ios-compose"></i>
            <span>Valida as informações do pedido de compra com a Nota Fiscal eletrônica</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="block" style="padding-top: 0;">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-push-3 col-sm-10 col-sm-push-1 text-center">
          <h2>INCRIVELMENTE SIMPLES</h2>
          <h3>Só no 99Kote você tem acesso a incríveis dashboards com informações que importam para você!</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="slider-screens">
            <div class="row">
              <div class="col-sm-1 hidden-xs">
              <i id="slider-screens-prev" class="ion-chevron-left"></i>
              </div>
              <div class="col-sm-10">
                <div class="browser">
                  <img src="<?php bloginfo('template_url'); ?>/img/navbar.png" alt="Barra de Navegação" class="img-navbar">
                  <div id="slider-screens">
                    <?php foreach (CFS()->get('dashboards', $page->ID) as $dash) : ?>
                      <div><img src="<?php echo wp_get_attachment_url( $dash['screenshot'] ); ?>" alt="<?php echo get_post_meta($dash['screenshot'], '_wp_attachment_image_alt', true); ?>"></div>
                    <?php endforeach; ?>
                  </div>
                </div>
                <!-- <div class="slick-dots"></div> -->
                <div id="slider-screens-for">
                  <?php foreach (CFS()->get('dashboards', $page->ID) as $dash) :  ?>
                    <div><p><?php echo $dash['caption']; ?></p></div>
                  <?php endforeach; ?>
                </div>
              </div>
              <div class="col-sm-1 hidden-xs">
                <i id="slider-screens-next" class="ion-chevron-right"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="block">
    <div class="container">
      <div class="col3">
        <div class="col3-row">
          <div class="col">
            <h3>Obtenha pedidos online e aprimore a gestão de vendas.</h3>
            <h4>A 99Kote faz parte do maior canal de vendas vertical do transporte.</h4>
            <h4>Cadastre sua empresa e aumente as vendas hoje!</h4>
            <hr>
            <h5>Só paga se Vender!</h5>
            <a href="https://99kote.globus7.com.br/CadastroUsuario" class="btn btn-success btn-block">CADASTRE-SE</a>
            <p>Não é preciso cadastrar nenhum cartão de crédito ou qualquer outro meio de pagamento no período de experimentação.</p>
          </div>
          <div class="col">
            <ul>
              <li>
                <i class="ion-android-lock"></i>
                <div>
                  <h4>Segurança</h4>
                  <p>Sabemos da importância de manter segura as informações de sua empresa. Temos SSL para todo o ambiente.</p>
                </div>
              </li>
              <li>
                <i class="ion-android-chat"></i>
                <div>
                  <h4>Suporte Especializado</h4>
                  <p>Equipe de suporte especializada. Estamos preparados para tirar todas as suas dúvidas.</p>
                </div>
              </li>
              <li>
                <i class="ion-android-cloud"></i>
                <div>
                  <h4>Cloud</h4>
                  <p>Nossa ferramenta é desenvolvida e suportada pelas gigantes da tecnologia. Estamos 100% na nuvem.</p>
                </div>
              </li>
            </ul>
          </div>
          <div class="col" style="background-image: url(<?php bloginfo('template_url'); ?>/img/so-paga-se-usar.jpg);"></div>
        </div>
      </div>
    </div>
  </div>
  <?php get_template_part('partials/content', 'empresas'); $page = get_post(11); ?>
  <div class="block">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-push-3 text-center">
          <h2>AGORA O MAIS INCRÍVEL DE TUDO. <strong>VOCÊ SÓ PAGA SE VENDER!</strong></h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10 col-md-push-1 text-center">
          <h3>
            Não precisa cadastrar cartão de crédito ou qualquer outro meio de pagamento. <br>
            Com o 99Kote sua empresa só paga pelas transações concretizadas. <br>
            O valor será dimensionado conforme a faixa de venda.
          </h3>

          <div style="margin-bottom: 35px;"></div>

          <div class="price">
            <?php echo $page->post_content; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php get_template_part('partials/content', 'news'); ?>
  <div class="block">
    <div class="box-itens bg-21b5ea">
      <div class="container">
        <div class="row">
          <div class="col-sm-3">
            <div class="item">
              <i class="ion-person-stalker"></i>
              <span><?php echo CFS()->get('box_1', $page->ID); ?></span>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="item">
              <i class="ion-card"></i>
              <span><?php echo CFS()->get('box_2', $page->ID); ?></span>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="item">
              <i class="ion-android-bus"></i>
              <span><?php echo CFS()->get('box_3', $page->ID); ?></span>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="item">
              <i class="ion-ios-chatboxes"></i>
              <span><?php echo CFS()->get('box_4', $page->ID); ?></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
