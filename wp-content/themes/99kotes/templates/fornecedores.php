<?php
/*
* Template name: Fornecedores
*/
get_header();
the_post();
 ?>
 <section>
   <div class="nav-btns">
     <div class="container">
       <div class="row">
         <div class="col-sm-12">
           <h1><?php the_title(); ?></h1>
         </div>
         <div class="col-sm-12">
           <div class="nav-btns-content">
             <div class="nav-btns-row">
               <?php $my_wp_query = new WP_Query();
               $main_page =  get_page(22);
                      $all_wp_pages = $my_wp_query->query(array('post_type' => 'page', 'posts_per_page' => '-1'));
                      $childs = get_page_children($main_page->ID, $all_wp_pages);
              ?>
               <?php foreach($childs as $child): ?>
                <a href="<?php echo get_permalink($child->ID); ?>" class="btn btn-info text-uppercase <?php echo $child->ID == $post->ID ? 'active' : null; ?>"><?php echo $child->post_title; ?></a>
              <?php endforeach; wp_reset_postdata(); ?>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
   <div class="block">
     <div class="container">
       <div class="features">
         <div class="text-center">
           <h2><?php echo CFS()->get('subtitulo'); ?></h2>
           <h3><?php echo CFS()->get('info'); ?></h3>
         </div>
         <div class="box-features">
           <div class="row">
             <div class="col-sm-4">
               <?php echo CFS()->get('descritivo'); ?>
             </div>
             <div class="col-sm-8">
               <div class="slider-screens-2">
                 <div class="content">
                   <div class="box-slider">
                     <div id="slider-screens-2">
                       <?php foreach (CFS()->get('banners') as $banner) : ?>
                         <div><img src="<?php echo wp_get_attachment_url( $banner['imagem'] ); ?>" alt="<?php echo get_post_meta($banner['imagem'], '_wp_attachment_image_alt', true); ?>"></div>
                       <?php endforeach; ?>
                     </div>
                   </div>
                   <a href="#" id="slider-screens-prev-2" class="slider-screens-2-arrow" style="display: <?php echo count( CFS()->get('banners') ) < 2 ? 'none' : null; ?>;"><i class="ion-ios-arrow-back"></i></a>
                   <a href="#" id="slider-screens-next-2" class="slider-screens-2-arrow" style="display: <?php echo count( CFS()->get('banners') ) < 2 ? 'none' : null; ?>;"><i class="ion-ios-arrow-forward"></i></a>
                 </div>
                 <img src="<?php bloginfo('template_url'); ?>/img/screen-2.png" class="screen" alt="Telas 99kote" draggable="false">

                   <?php if(CFS()->get('banners')[0] !== null && (CFS()->get('banners')[0]['legenda_titulo'] !== null || CFS()->get('banners')[0]['legenda_descricao'] !== null)): ?>
                     <div id="slider-screens-for-2" class="description">
                      <?php foreach (CFS()->get('banners') as $banner) : ?>

                         <div>
                           <h6><?php echo $banner['legenda_titulo']; ?></h6>
                           <p><?php echo $banner['legenda_descricao']; ?></p>
                         </div>
                      <?php endforeach; ?>
                     </div>
                   <?php endif; ?>

               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
     <div class="box-text-color bg-1d2149">
       <div class="container">
         <div class="row">
           <div class="col-sm-10 col-sm-push-1">
             <?php the_content(); ?>
           </div>
         </div>
       </div>
     </div>
     <div class="box-cta-2" style="padding-top: 50px;">
       <div class="container">
         <div class="row">
           <div class="col-md-6">
             <h2>Receba cotações hoje!</h2>
           </div>
           <div class="col-md-6">
             <a href="https://99kote.globus7.com.br/CadastroUsuario" class="btn btn-success btn-lg">CADASTRE-SE</a>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <?php get_footer(); ?>
