<?php
/*
* Template Name: Ajuda
*/
get_header();
the_post();
?>
<section>
  <div class="banner inter help" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'large')[0]; ?>);">
    <div class="vcenter">
      <div class="container">
        <div class="content">
          <h1><?php the_title(); ?></h1>
        </div>
      </div>
    </div>
  </div>
  <div class="block">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-push-1">
          <div class="help">
            <?php the_content(); ?>

            <h3 class="accordion-title">Dúvidas Gerais</h3>
            <div class="panel-group accordion" id="accordion">
                <?php foreach (CFS()->get('duvidas_gerais') as $k => $fac) : ?>
                <div class="panel panel-primary">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse_1_<?php echo $k; ?>"><?php echo $fac['pergunta']; ?></a>
                    </h4>
                  </div>
                  <div id="collapse_1_<?php echo $k; ?>" class="panel-collapse collapse">
                      <div class="panel-body"><?php echo $fac['resposta']; ?></div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>

            <h3 class="accordion-title">Dúvidas Compradores</h3>
            <div class="panel-group accordion" id="accordion2">
                <?php foreach (CFS()->get('duvidas_compradores') as $k => $fac) : ?>
                <div class="panel panel-primary">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_<?php echo $k; ?>"><?php echo $fac['pergunta']; ?></a>
                    </h4>
                  </div>
                  <div id="collapse_2_<?php echo $k; ?>" class="panel-collapse collapse">
                      <div class="panel-body"><?php echo $fac['resposta']; ?></div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>

            <h3 class="accordion-title">Dúvidas Fornecedores</h3>
            <div class="panel-group accordion" id="accordion3">
                <?php foreach (CFS()->get('duvidas_fornecedores') as $k => $fac) : ?>
                <div class="panel panel-primary">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_<?php echo $k; ?>"><?php echo $fac['pergunta']; ?></a>
                    </h4>
                  </div>
                  <div id="collapse_3_<?php echo $k; ?>" class="panel-collapse collapse">
                      <div class="panel-body"><?php echo $fac['resposta']; ?></div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>


          </div>
        </div>
      </div>
    </div>
  </div>
<?php get_template_part('partials/content', 'news'); ?>
  <div style="margin-bottom: 50px;"></div>
</section>
<?php get_footer(); ?>
