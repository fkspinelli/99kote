<?php
/*
* Template name: Planos
*/
get_header();
the_post();
 ?>
 <section>

  <div class="block">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-push-3 text-center">
          <h2>AGORA O MAIS INCRÍVEL DE TUDO. <strong>VOCÊ SÓ PAGA SE VENDER!</strong></h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-10 col-md-push-1 text-center">
          <h3>
            Não precisa cadastrar cartão de crédito ou qualquer outro meio de pagamento. <br>
            Com o 99Kote sua empresa só paga pelas transações concretizadas. <br>
            O valor será dimensionado conforme a faixa de venda.
          </h3>

          <div style="margin-bottom: 35px;"></div>

          <div class="price">
            <?php
                $query = new WP_Query( 'pagename=homepage' );
                while ( $query->have_posts() ) : $query->the_post();
                    the_content();
                endwhile;
                wp_reset_postdata();
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>

   <div class="block">
     <div class="box-text-color bg-1d2149">
       <div class="container">
         <div class="row">
           <div class="col-sm-10 col-sm-push-1">
             <?php the_content(); ?>
           </div>
         </div>
       </div>
     </div>
     <div class="box-cta-2" style="padding-top: 50px;">
       <div class="container">
         <div class="row">
           <div class="col-md-6">
             <h2>Receba cotações hoje!</h2>
           </div>
           <div class="col-md-6">
             <a href="https://99kote.globus7.com.br/CadastroUsuario" class="btn btn-success btn-lg">CADASTRE-SE</a>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <?php get_footer(); ?>
