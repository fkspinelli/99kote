<?php
/*
* Template Name: Contato
*/
get_header();
the_post();
?>
<section>
  <div class="banner inter" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'large')[0]; ?>);">
    <div class="vcenter">
      <div class="container">
        <div class="content">
          <h1>Contato</h1>
        </div>
      </div>
    </div>
  </div>
  <div class="block">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-push-6">
          <?php echo do_shortcode('[contact-form-7 id="40" title="Formulário de contato"]'); ?>
        </div>
        <div class="col-md-6 col-md-pull-6">
          <div class="panel panel-custom panel-primary contacts">
            <div class="panel-heading">
              <h4>Atendimento</h4>
            </div>
            <div class="panel-body">
              <div class="contact-info">
              <?php the_content(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
