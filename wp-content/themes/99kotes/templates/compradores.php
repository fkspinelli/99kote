<?php
/*
* Template Name: Compradores
*/
get_header();
the_post();
?>
<section>
  <div class="banner inter" style="background-image: url(<?php echo wp_get_attachment_image_src(get_post_thumbnail_id(), 'large')[0]; ?>);">
    <div class="vcenter">
      <div class="container">
        <div class="content">
          <h1><?php the_title(); ?></h1>
          <h2>O ponto de encontro do comércio eletrônico para a compra e venda de peças e serviços.</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="box-text-color bg-1d2149">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-push-1">
          <?php the_content(); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="block def-cl-1">
    <div class="container">
      <div class="item-box">
        <div class="row">
          <div class="col-sm-8 col-sm-push-2">
            <h2>Como <b>99kote</b> pode te ajudar</h2>
          </div>
        </div>
        <div class="itens">
          <div class="item">
            <i class="ion-social-usd"></i>
            <span>Fácil visualização da quantidade de cotação realizadas</span>
          </div>
          <div class="item">
            <i class="ion-android-list"></i>
            <span>Apresentação simples das melhores opções para cada operação analisada</span>
          </div>
          <div class="item">
            <i class="ion-ios-monitor"></i>
            <span>Automatização de processos</span>
          </div>
          <div class="item">
            <i class="ion-ios-compose"></i>
            <span>Fim das análises “manuais”</span>
          </div>
          <div class="item">
            <i class="ion-ios-people"></i>
            <span>Possibilidade de realização de cotações públicas</span>
          </div>
          <div class="item">
            <i class="ion-android-cart"></i>
            <span>Integração ao ERP  Módulo de Compras</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="block bg-f1f1f1">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="text-center">
            <h2>CONHEÇA ALGUNS DOS NOSSOS FORNECEDORES</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="logos logos-5">
            <?php foreach (CFS()->get('fornecedores') as $f) : ?>
              <div class="logos-box">
                <img src="<?php echo wp_get_attachment_url( $f['fornecedor_imagem'] ); ?>" alt="<?php echo get_post_meta($f['fornecedor_imagem'], '_wp_attachment_image_alt', true); ?>">
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="block">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-push-1">
          <div class="box-cta">
            <h3>Não é cliente do software Globus? </h3>
            <p>Clique abaixo e solicite um contato</p>
            <a href="http://materiais.bgmrodotec.com.br/agendar-demonstracao" class="btn btn-info text-uppercase">SOLICITAR CONTATO</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>
